﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;

namespace WEB.Controllers
{
    public class HomeController : Controller
    {
        PruebasEntities pe = new PruebasEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListaEmpleados()
        {
            var lista = pe.sp_Lista_Empleados().ToList();
            return View(lista);
        }

        public ActionResult NuevoEmpleado()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NuevoEmpleado(string CodigoEmpleado, string NombreEmpleado, string ApellidoEmpleado, int EdadEmpleado, string CorreoEmpleado)
        {
            try
            {
                pe.sp_Nuevo_Empleado(CodigoEmpleado, NombreEmpleado, ApellidoEmpleado, EdadEmpleado, CorreoEmpleado);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}