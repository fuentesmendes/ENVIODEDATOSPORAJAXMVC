﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MODELO
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PruebasEntities : DbContext
    {
        public PruebasEntities()
            : base("name=PruebasEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<T_Empleado> T_Empleado { get; set; }
    
        public virtual ObjectResult<sp_Lista_Empleados_Result> sp_Lista_Empleados()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Lista_Empleados_Result>("sp_Lista_Empleados");
        }
    
        public virtual int sp_Nuevo_Empleado(string cod, string nom, string apell, Nullable<int> eda, string correo)
        {
            var codParameter = cod != null ?
                new ObjectParameter("Cod", cod) :
                new ObjectParameter("Cod", typeof(string));
    
            var nomParameter = nom != null ?
                new ObjectParameter("Nom", nom) :
                new ObjectParameter("Nom", typeof(string));
    
            var apellParameter = apell != null ?
                new ObjectParameter("Apell", apell) :
                new ObjectParameter("Apell", typeof(string));
    
            var edaParameter = eda.HasValue ?
                new ObjectParameter("Eda", eda) :
                new ObjectParameter("Eda", typeof(int));
    
            var correoParameter = correo != null ?
                new ObjectParameter("Correo", correo) :
                new ObjectParameter("Correo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Nuevo_Empleado", codParameter, nomParameter, apellParameter, edaParameter, correoParameter);
        }
    }
}
